<?php

namespace App\Repositories\Post;

use App\Models\Post;

class PostRepository implements PostRepositoryInterface
{

    public function getAll()
    {
        return Post::all();
    }

    public function get($id)
    {
        return Post::Find($id);
    }

    public function create($request)
    {
        return "this is create | PostRepository";
    }

    public function update($request, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }
}
