<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use DB;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++){
        DB::table('posts')->insert([
            'title' => 'This is title : ' . Str::random(10),
            'description' => 'This is title description: ' . Str::random(10),
            'slug' => Str::random(10),
            'content' => "this is content,  this is content,this is content,this is content, " . Str::random(10),
        ]);
    }}
}
