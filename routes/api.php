<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('/login', 'App\Http\Controllers\AuthController@login');
    Route::post('/register', 'App\Http\Controllers\AuthController@register');
    Route::post('/logout', 'App\Http\Controllers\AuthController@logout');
    Route::post('/refresh', 'App\Http\Controllers\AuthController@refresh');
    Route::get('/user-profile', 'App\Http\Controllers\AuthController@userProfile');
    Route::post('/change-pass', 'App\Http\Controllers\AuthController@changePassWord');
});

Route::group(['middleware' => ['jwt.verify']], function () {
    Route::get('/post', 'App\Http\Controllers\PostController@index')->middleware('can:viewAny,App\Models\Post');
    Route::get('/post/{id}/show', 'App\Http\Controllers\PostController@show')->middleware('can:viewAny,App\Models\Post');
    Route::post('/post/create', 'App\Http\Controllers\PostController@store')->middleware('can:create,App\Models\Post');
    Route::post('/post/{id}/delete', 'App\Http\Controllers\PostController@destroy')->middleware('can:delete,App\Models\Post');
    Route::post('/post/search', 'App\Http\Controllers\PostController@search')->middleware('can:search,App\Models\Post');

    Route::post('/post/createNewPost', 'App\Http\Controllers\PostController@createNewPost')->middleware('can:create,App\Models\Post');

    //// GATE
    Route::post('/post/testgate', 'App\Http\Controllers\PostController@testGate')->middleware('can:update-setting');
});


